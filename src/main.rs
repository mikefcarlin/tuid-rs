extern crate tuid;
#[macro_use]
extern crate clap;
extern crate chrono;

use clap::{SubCommand, Arg, App};
use tuid::{Tuid, decode, as_json};

pub fn main() {
	let matches = App::new("Tuid")
        .version("0.1.0")
        .author("Mike Carlin <mikefcarlin@protonmail.com")
        .subcommand(SubCommand::with_name("decode")
                                      .visible_alias("d")
                                      .about("Provides the ability to decode a TUID")
                                      .arg(Arg::with_name("as_json")
                                          .short("j")
                                          .long("as-json")
                                          .help("Indicates that we should decode this arg"))
                                      .arg(Arg::with_name("INPUT")
                                           .help("tuid to use with a subcommand")
                                           .required(true)))
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("decode") {
        if matches.is_present("INPUT") {
            let input = value_t_or_exit!(matches.value_of("INPUT"), i64);
            let tuid: Tuid = decode(input);
            if matches.is_present("as_json") {
                println!("{}", as_json(tuid));
            } else {
                println!("DateTime (UTC): {}, DateTime (Local): {}, Counter: {}, Server ID: {}",
                         tuid.date_time_utc,
                         tuid.date_time_local,
                         tuid.counter,
                         tuid.server_id);
            }
        } 
    }
}


