extern crate chrono;

use chrono::{TimeZone, NaiveDateTime, DateTime};
use chrono::prelude::Local;

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

#[derive(Serialize)]
pub struct Tuid {
    pub epoch_ms: i64,
    pub date_time_utc: NaiveDateTime,
    pub date_time_local: DateTime<Local>,
    pub counter: u16,
    pub server_id: u8,
}

pub fn as_json(tuid: Tuid) -> String {
    serde_json::to_string(&tuid).unwrap()
}

pub fn decode(tuid: i64) -> Tuid {
    let date_time_utc = NaiveDateTime::from_timestamp((tuid >> 18) / 1000, 0);
    let date_time_local: DateTime<Local> = Local.from_utc_datetime(&date_time_utc);
    Tuid {
        epoch_ms: tuid >> 18,
        date_time_utc,
        date_time_local,
        counter: (tuid >> 8 & 0x3FF) as u16,
        server_id: tuid as u8,
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_decode() {
        let tuid = decode(398718987589650375);
        assert_eq!(tuid.epoch_ms, 1520992231711);
        assert_eq!(tuid.counter, 7);
        assert_eq!(tuid.server_id, 199);
    }

    #[test]
    fn test_decode_zero() {
        let tuid = decode(0);
        assert_eq!(tuid.epoch_ms, 0);
        assert_eq!(tuid.counter, 0);
        assert_eq!(tuid.server_id, 0);
    }
}
